import re

# get total time from -log_view file
def get_runtime(file_name):
	file = open(file_name, "r")
	for line in file:
		if "Time (sec)" in line:
			time = float(re.findall("\d+\.\d+[eE][+-]\d+", line)[0])
			return time
	return float('nan') # CUDA error

# get flop rate from -log_view file
def get_floprate(file_name):
	file = open(file_name, "r")
	for line in file:
		if "Flop/sec:" in line:
			time = float(re.findall("\d+\.\d+[eE][+-]\d+", line)[2])
			return time
	return float('nan') # CUDA error

# get Main Stage flop rate from -log_view file
def get_mainstage(file_name):
	file = open(file_name, "r")
	for line in file:
		if "Main Stage" in line:
			line = re.findall("\d+\.\d+[eE][+-]\d+", line)
			time = float(line[0])
			flops = float(line[1])
			return flops/time
	return float('nan') # CUDA error

# get VecDot flop rate from -log_view file
def get_VecDot(file_name, which=1):
	file = open(file_name, "r")
	for line in file:
		if "VecDot" in line:
			if which == 1:
				line = line.split(" ")
				return line[-1]
			else:
				which -= 1
				continue
	return float('nan') # CUDA error


