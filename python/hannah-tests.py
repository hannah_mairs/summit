import matplotlib.pyplot as plt
import hannah_utils as ut

# Does multi-process service help?
# x-axis: number of GPUs
# y-axis VecDot flop rate
# lines: # of MPI ranks/GPU (1, 2)

def graph1():

	gpus = range(1, 7)
	runs = [351467, 351470, 351761, 351764, 351765, 351675]

	# get flop rates
	rank1 = [] # one rank per GPU
	rank2 = [] # two ranks per GPU
	rank4 = []
	for i in range(len(gpus)):
		rank1.append(float(ut.get_VecDot("../data/vec_ex43/vec_ex43.n" + str(gpus[i]) + "_g1_c1.1000000." + str(runs[i]))))
		rank2.append(float(ut.get_VecDot("../data/vec_ex43/vec_ex43.n" + str(gpus[i]) + "_g1_c2.1000000." + str(runs[i]))))
		rank4.append(float(ut.get_VecDot("../data/vec_ex43/vec_ex43.n" + str(gpus[i]) + "_g1_c4.1000000." + str(runs[i]))))

	# plot
	plt.plot(gpus, rank1, color="grey", label="One MPI rank per GPU")
	plt.plot(gpus, rank2, color="firebrick", label="Two MPI ranks per GPU")
	plt.plot(gpus, rank4, color="steelblue", label="Four MPI ranks per GPU")

	plt.title("Multi-process service on Summit, Vec size $10^6$", fontsize=16)
	plt.xlabel("Number of GPUs", fontsize=16)
	plt.ylabel("VecDot MFlops/second", fontsize=16)
	plt.legend(loc="upper left", fontsize=16)
	plt.xlim([1, 6])
	plt.tight_layout()

	# plt.show()
	plt.savefig("../plots/vec_ex43_MPS_10_6_r1_r2_r4.png")
	plt.gcf().clear()

# graph1()

# Do bigger vectors utilize the GPU better?
def graph2():

	gpus = range(1, 7)
	runs = [351467, 351470, 351761, 351764, 351765, 351675]
	sizes = [10000, 1000000, 100000000]

	# get flop rates
	vecsize1 = []
	vecsize2 = []
	vecsize3 = []
	for i in range(len(gpus)):
		vecsize1.append(float(ut.get_VecDot("../data/vec_ex43/vec_ex43.n" + str(gpus[i]) + "_g1_c1." + str(sizes[0]) + "." + str(runs[i]))))
		vecsize2.append(float(ut.get_VecDot("../data/vec_ex43/vec_ex43.n" + str(gpus[i]) + "_g1_c1." + str(sizes[1]) + "." + str(runs[i]))))
		vecsize3.append(float(ut.get_VecDot("../data/vec_ex43/vec_ex43.n" + str(gpus[i]) + "_g1_c1." + str(sizes[2]) + "." + str(runs[i]))))

	# plot
	plt.plot(gpus, vecsize1, color="grey", label="Vec size $10^4$")
	plt.plot(gpus, vecsize2, color="firebrick", label="Vec size $10^6$")
	plt.plot(gpus, vecsize3, color="steelblue", label="Vec size $10^8$")

	plt.title("Problem size on Summit, Vec test ex43", fontsize=16)
	plt.xlabel("Number of GPUs", fontsize=16)
	plt.ylabel("VecDot MFlops/second", fontsize=16)
	plt.legend(loc="upper left", fontsize=16)
	plt.xlim([1, 6])
	plt.tight_layout()

	# plt.show()
	plt.savefig("../plots/vec_ex43_vec_size.png")
	plt.gcf().clear()

graph2()







