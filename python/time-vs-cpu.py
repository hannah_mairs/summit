# plot time vs CPUs for fixed number of GPUs

import matplotlib.pyplot as plt
import hannah_utils as ut

def make_plot(directory_prefix, file_prefix, run_num, num_cpus, num_unknowns, labels, plot_title, save_plot):

	all_runs = []
	for i in range(len(num_unknowns)): 
		runtimes = []
		for j in range(len(num_cpus[i])):
			time = ut.get_runtime(directory_prefix[i] + file_prefix[i] + str(num_cpus[i][j]) + "." + str(num_unknowns[i]) + "." + str(run_num[i]))
			runtimes.append(time)
		all_runs.append(runtimes)

	num = len(all_runs)
	cm = plt.get_cmap('Greys')
	fig = plt.figure()
	ax = fig.add_subplot(111)
	ax.set_color_cycle([cm((1.*i+1)/num) for i in range(num)])
	for i in range(num):
		ax.plot(num_cpus[0], all_runs[i], marker="o", linestyle="none", label=labels[i])

	plt.title(plot_title, fontsize=16)
	plt.xlabel("Number of CPUs", fontsize=16)
	plt.ylabel("Time (sec)", fontsize=16)
	plt.legend(loc="upper left", fontsize=16)
	plt.xlim([0, 43])
	plt.tight_layout()
	# plt.show()

	plt.savefig(save_plot)
	plt.gcf().clear()


# 1 GPU
# make_plot(["../data/vec-ex43_g1/"]*5, ["vec_ex43.n1_g1_c"]*5, [351467]*5, [range(1, 43)]*5, [1000, 10000, 100000, 1000000, 10000000], ["$10^3$", "$10^4$", "$10^5$", "$10^6$", "$10^7$"], "Time for vector operations ex43 on one GPU", "../plots/vec_ex43_g1.png")
# make_plot(["../data/vec-ex43_g1-gpfs/"]*5, ["vec_ex43.n1_g1_c"]*5, [350831]*5, [range(1, 43)]*5, [1000, 10000, 100000, 1000000, 10000000], ["$10^3$", "$10^4$", "$10^5$", "$10^6$", "$10^7$"], "Time for vector operations ex43 on one GPU", "../plots/vec_ex43_g1-gpfs.png")

# 2 GPUs
# make_plot(["../data/vec-ex43_g2/"]*5, ["vec_ex43.n1_g2_c"]*5, [351469]*5, [range(1, 43)]*5, [1000, 10000, 100000, 1000000, 10000000], ["$10^3$", "$10^4$", "$10^5$", "$10^6$", "$10^7$"], "Time for vector operations ex43 on two GPUs", "../plots/vec_ex43_g2.png")
# make_plot(["../data/vec-ex43_g2-gpfs/"]*5, ["vec_ex43.n1_g2_c"]*5, [351325]*5, [range(1, 43)]*5, [1000, 10000, 100000, 1000000, 10000000], ["$10^3$", "$10^4$", "$10^5$", "$10^6$", "$10^7$"], "Time for vector operations ex43 on two GPUs", "../plots/vec_ex43_g2-gpfs.png")

# 3, 4, 5, 6 GPUs
# make_plot(["../data/vec-ex43_g3/"]*5, ["vec_ex43.n1_g3_c"]*5, [351760]*5, [range(1, 43)]*5, [1000, 10000, 100000, 1000000, 10000000], ["$10^3$", "$10^4$", "$10^5$", "$10^6$", "$10^7$"], "Time for vector operations ex43 on three GPUs", "../plots/vec_ex43_g3.png")
# make_plot(["../data/vec-ex43_g4/"]*5, ["vec_ex43.n1_g4_c"]*5, [351648]*5, [range(1, 43)]*5, [1000, 10000, 100000, 1000000, 10000000], ["$10^3$", "$10^4$", "$10^5$", "$10^6$", "$10^7$"], "Time for vector operations ex43 on four GPUs", "../plots/vec_ex43_g4.png")
# make_plot(["../data/vec-ex43_g5/"]*5, ["vec_ex43.n1_g5_c"]*5, [351649]*5, [range(1, 43)]*5, [1000, 10000, 100000, 1000000, 10000000], ["$10^3$", "$10^4$", "$10^5$", "$10^6$", "$10^7$"], "Time for vector operations ex43 on five GPUs", "../plots/vec_ex43_g5.png")
# make_plot(["../data/vec-ex43_g6/"]*5, ["vec_ex43.n1_g6_c"]*5, [351651]*5, [range(1, 43)]*5, [1000, 10000, 100000, 1000000, 10000000], ["$10^3$", "$10^4$", "$10^5$", "$10^6$", "$10^7$"], "Time for vector operations ex43 on six GPUs", "../plots/vec_ex43_g6.png")

# 1 and 2 RS on 2 GPUs
# make_plot(["../data/vec-ex43_g2/"]*2, ["vec_ex43.n1_g2_c", "vec_ex43.n2_g1_c"], [351469, 351470], [range(2, 43, 2), range(1, 22)], [1000000]*2, ["1 resource set", "2 resource sets"], "Time for ex43 on 2 GPUs", "../plots/vec_ex43_g2_resource_set.png")

# 1 and 3 RS on 3 GPUs
# make_plot(["../data/vec-ex43_g3/"]*2, ["vec_ex43.n1_g3_c", "vec_ex43.n3_g1_c"], [351760, 351761], [range(3, 43, 3), range(1, 15)], [1000000]*2, ["1 resource set", "3 resource sets"], "Time for ex43 on 3 GPUs", "../plots/vec_ex43_g3_resource_set.png")

# NSF and GPFS on 1 GPU
make_plot(["../data/vec-ex43_g1/", "../data/vec-ex43_g1-gpfs/"], ["vec_ex43.n1_g1_c"]*2, [351467, 350831], [range(1, 43)]*2, [1000000]*2, ["NFS", "GPFS"], "Time for ex43 on one GPU", "../plots/vec_ex43_g1_file_system.png")

# NSF and GPFS on 2 GPUs
make_plot(["../data/vec-ex43_g2/", "../data/vec-ex43_g2-gpfs/"], ["vec_ex43.n1_g2_c"]*2, [351469, 351325], [range(1, 43)]*2, [1000000]*2, ["NFS", "GPFS"], "Time for ex43 on two GPUs", "../plots/vec_ex43_g2_file_system.png")


