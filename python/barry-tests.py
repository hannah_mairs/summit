import matplotlib.pyplot as plt
import hannah_utils as ut
import re
import sys

# experiment 1: CPU + GPU performance
# y-axis: flop rate
# x-asix: GPUs with one CPU per GPU

def graph1():

	gpus = range(1, 7)

	# get flop rates
	flop_rates = []
	for gpu in gpus:
		# rate = ut.get_mainstage("../data/vec_ex43/vec_ex43.n1_g" + str(gpu) + "_c" + str(gpu) + ".100000000.368397") 
		rate = ut.get_VecDot("../data/vec_ex43/vec_ex43.n1_g" + str(gpu) + "_c" + str(gpu) + ".100000000.368397")
		flop_rates.append(float(rate))

	# plot
	plt.plot(gpus, flop_rates, color="firebrick", label="Vec size $10^8$")

	plt.title("Vec test ex43 performance on Summit", fontsize=16)
	plt.xlabel("Number of GPUs", fontsize=16)
	plt.ylabel("Total Flops/second", fontsize=16)
	# plt.ylabel("VecDot MFlops/second", fontsize=16)
	plt.legend(loc="upper left", fontsize=16)
	plt.xlim([1, 6])
	plt.tight_layout()

	# plt.show()
	# plt.savefig("../plots/vec_ex43_GPU_mainstage_flop_rate.png")
	plt.savefig("../plots/vec_ex43_GPU_VecDot_flop_rate.png")
	plt.gcf().clear()

# make the graph
# graph1()

def graph2():

	gpus = range(1, 7)

	# get flop rates
	flop_rates1 = []
	flop_rates2 = []
	for gpu in gpus:
		rate1 = ut.get_VecDot("../data/vec_ex43/vec_ex43.n1_g" + str(gpu) + "_c" + str(gpu) + ".100000000.368397") 
		rate2 = ut.get_VecDot("../data/vec_ex43/vec_ex43.n" + str(gpu) + "_g1_c1.100000000.368405")
		flop_rates1.append(float(rate1))
		flop_rates2.append(float(rate2))

	# plot
	plt.plot(gpus, flop_rates1, color="firebrick", marker="x", markersize="10", markeredgewidth=2, linestyle="none", label="1 resource set")
	plt.plot(gpus, flop_rates2, color="steelblue", marker="o", markersize="10", markeredgewidth=2, markerfacecolor='none', linestyle="none", label="multiple resource sets")

	plt.title("Vec test ex43 size $10^8$ performance on Summit", fontsize=16)
	plt.xlabel("Number of GPUs", fontsize=16)
	plt.ylabel("VecDot MFlops/second", fontsize=16)
	plt.legend(loc="upper left", fontsize=16)
	plt.tight_layout()

	# plt.show()
	plt.savefig("../plots/vec_ex43_GPU_VecDot_flop_rate_resource_set.png")
	plt.gcf().clear()

# graph2()

def graph3():

	gpus = range(1, 7)

	# get flop rates
	difference = []
	for gpu in gpus:
		rate1 = ut.get_VecDot("../data/vec_ex43/vec_ex43.n1_g" + str(gpu) + "_c" + str(gpu) + ".100000000.368397") 
		rate2 = ut.get_VecDot("../data/vec_ex43/vec_ex43.n" + str(gpu) + "_g1_c1.100000000.368405")
		difference.append(float(rate2) - float(rate1))

	# plot
	plt.plot(gpus, difference, color="firebrick", marker="x", markersize="10", markeredgewidth=2, linestyle="none")
	plt.plot(gpus, [0]*len(gpus), color="grey", linestyle="dashed", linewidth=2)

	plt.title("Multiple vs one resource set on Summit", fontsize=16)
	plt.xlabel("Number of GPUs", fontsize=16)
	plt.ylabel("MFlops/second", fontsize=16)
	# plt.xlim([1, 6])
	plt.tight_layout()

	# plt.show()
	plt.savefig("../plots/vec_ex43_GPU_VecDot_flop_rate_resource_set_diff.png")
	plt.gcf().clear()

# graph3()

# experiment 2: CPU only performance
# y-axis: flop rate
# x-axis: MPI ranks

def graph4():

	ranks = range(1, 27) # what I have so far

	flop_rates = []
	for rank in ranks:
		rate = ut.get_VecDot("../data/vec_ex43/vec_ex43.n2_g0_c21_a" + str(rank) + ".100000000.374957")
		flop_rates.append(float(rate))

	ranks = range(27, 35)
	for rank in ranks:
		rate = ut.get_VecDot("../data/vec_ex43/vec_ex43.n2_g0_c21_a" + str(rank) + ".100000000.378889")
		flop_rates.append(float(rate))

	ranks = range(35, 39)
	for rank in ranks:
		rate = ut.get_VecDot("../data/vec_ex43/vec_ex43.n2_g0_c21_a" + str(rank) + ".100000000.379392")
		flop_rates.append(float(rate))

	ranks = range(39, 42)
	for rank in ranks:
		rate = ut.get_VecDot("../data/vec_ex43/vec_ex43.n2_g0_c21_a" + str(rank) + ".100000000.379603")
		flop_rates.append(float(rate))

	rate = ut.get_VecDot("../data/vec_ex43/vec_ex43.n2_g0_c21_a42.100000000.379714")


	ranks = range(1, 42)

	# plot
	plt.plot(ranks, flop_rates, color="firebrick", marker="o", markeredgewidth=2, markerfacecolor='none', linestyle="none", label="Vec size $10^8$")
	plt.axvline(x=21, color="grey", linestyle="dashed", linewidth=2, label="1 rank per core")

	plt.title("Vec test ex43 CPU performance on Summit", fontsize=16)
	plt.xlabel("Number of MPI ranks", fontsize=16)
	plt.ylabel("VecDot Flops/second", fontsize=16)
	plt.legend(loc="lower right", fontsize=16)
	plt.tight_layout()
	plt.xlim([0, 42])

	# change x-axis labels
	locations = range(0, 45, 5)
	labels = []
	for loc in locations:
		labels.append(str(2*loc))
	locations[0] = 1
	labels[0] = "2"
	plt.xticks(locations, labels)

	# plt.show()
	plt.savefig("../plots/vec_ex43_CPU_flop_rate.png")
	plt.gcf().clear()

# graph4()

def graph6():

	flop_rates = []

	ranks = range(1, 22)
	for rank in ranks:
		rate = ut.get_VecDot("../data/vec_ex43/vec_ex43.n2_g0_c21_a" + str(rank) + ".100000000.374957")
		flop_rates.append(float(rate))

	flop_rate42 = float(ut.get_VecDot("../data/vec_ex43/vec_ex43.n2_g0_c21_a42.100000000.379714"))

	# plot
	plt.plot(ranks, flop_rates, color="firebrick", marker="o", markeredgewidth=2, markerfacecolor='none', linestyle="none", label="Vec size $10^8$")
	plt.plot([42], [flop_rate42], color="firebrick", marker="o", markeredgewidth=2, markerfacecolor='none', linestyle="none")
	plt.axvline(x=21, color="grey", linestyle="dashed", linewidth=2, label="1 rank per core")

	plt.title("Vec test ex43 CPU performance on Summit", fontsize=16)
	plt.xlabel("Number of MPI ranks", fontsize=16)
	plt.ylabel("VecDot Flops/second", fontsize=16)
	plt.legend(loc="lower right", fontsize=16)
	plt.tight_layout()
	plt.xlim([0, 43])

	# change x-axis labels
	locations = range(0, 45, 5)
	labels = []
	for loc in locations:
		labels.append(str(2*loc))
	locations[0] = 1
	labels[0] = "2"
	plt.xticks(locations, labels)

	# plt.show()
	plt.savefig("../plots/vec_ex43_CPU_flop_rate_.png")
	plt.gcf().clear()

# graph6()

# experiment 3: CPU only vs CPU with GPU performance
# x-asix: MPI ranks (1-42)
# y-asix: VecDot flop rate
# line1: CPU only 
# line2: 6 GPU + CPU, 6 resource sets
# Vec size 10^7, ran out of memory on 6 GPUs with 10^8

def graph5():

	# get GPU + CPU data
	ranks = range(1, 8) # ranks per resource set
	cpus = range(1, 22)

	gpu_floprate = []
	for rank in ranks:
		gpu_floprate.append(float(ut.get_VecDot("../data/vec_ex43/vec_ex43.n6_g1_c" + str(rank) + ".1000000.351675")))
	cpu_floprate = []
	for cpu in cpus:
		cpu_floprate.append(float(ut.get_VecDot("../data/vec_ex43/vec_ex43.n2_g0_c" + str(cpu) + "_a" + str(cpu) + ".1000000.381405")))

	# plot
	plt.plot(range(2, 43, 2), cpu_floprate, color="steelblue", marker="o", markersize="6", markeredgewidth=2, markerfacecolor='none', linestyle="none", label="CPUs only")
	plt.plot(range(6, 43, 6), gpu_floprate, color="firebrick", marker="x", markersize="6", markeredgewidth=2, linestyle="none", label="6 GPUs with CPUs")

	plt.title("GPU vs CPU performance on Summit", fontsize=16) # Vec size $10^6$
	plt.xlabel("Number of MPI ranks", fontsize=16)
	plt.ylabel("VecDot MFlops/second", fontsize=16)
	plt.legend(loc="center right", fontsize=16)
	plt.tight_layout()

	# plt.show()
	plt.savefig("../plots/vec_ex43_CPU_vs_GPU.png")
	plt.gcf().clear()

# graph5()

# experiment 4: virtualization on Summit
# x-axis: MPI ranks per GPU
# y-axis: VecDot flops
# line 1: 1 GPU
# line 2: 2 GPUs
# Vec size 10^6

def graph7():

	ranks_per_gpu = range(1, 8)

	gpu1 = []
	gpu2 = []
	gpu4 = []
	gpu6 = []

	for ranks in ranks_per_gpu:
		gpu1.append(float(ut.get_VecDot("../data/vec_ex43/vec_ex43.n1_g1_c" + str(ranks) + ".10000.351467")))
		gpu2.append(float(ut.get_VecDot("../data/vec_ex43/vec_ex43.n2_g1_c" + str(ranks) + ".10000.351470")))
		gpu4.append(float(ut.get_VecDot("../data/vec_ex43/vec_ex43.n4_g1_c" + str(ranks) + ".10000.351764")))
		gpu6.append(float(ut.get_VecDot("../data/vec_ex43/vec_ex43.n6_g1_c" + str(ranks) + ".10000.351675")))

	# plot
	num = 4
	cm = plt.get_cmap('Greys')
	fig = plt.figure()
	ax = fig.add_subplot(111)
	ax.set_color_cycle([cm((1.*i+1)/num) for i in range(num)])

	ax.plot(ranks_per_gpu, gpu1, linewidth="2", label="1 GPU")
	ax.plot(ranks_per_gpu, gpu2, linewidth="2", label="2 GPUs")
	ax.plot(ranks_per_gpu, gpu4, linewidth="2", label="4 GPUs")
	ax.plot(ranks_per_gpu, gpu6, linewidth="2", label="6 GPUs")

	plt.title("Virtualization performance on Summit", fontsize=16) # Vec size $10^6$
	plt.xlabel("MPI ranks per GPU", fontsize=16)
	plt.ylabel("VecDot MFlops/second", fontsize=16)
	plt.legend(loc="center right", fontsize=16)
	plt.xlim([1, 7])
	plt.tight_layout()

	# plt.show()
	plt.savefig("../plots/vec_ex43_virtualization_10_4.png")
	plt.gcf().clear()

# graph7()

def graph8():

	ranks = range(1, 43)

	gpu6 = []
	for rank in ranks:
		gpu6.append(float(ut.get_VecDot("../data/vec_ex43/vec_ex43.n1_g6_c" + str(rank) + ".10000000.351651")))


	# plot
	plt.plot(ranks, gpu6, color="firebrick", marker="o", markersize="6", markeredgewidth=2, markerfacecolor='none', linestyle="none", label="6 GPUs")

	plt.title("Virtualization performance on Summit", fontsize=16) # Vec size $10^6$
	plt.xlabel("Number of MPI ranks", fontsize=16)
	plt.ylabel("VecDot MFlops/second", fontsize=16)
	plt.legend(loc="lower right", fontsize=16)
	plt.xlim([0, 43])
	plt.tight_layout()

	# plt.show()
	plt.savefig("../plots/vec_ex43_virtualization_g6_10_7.png")
	plt.gcf().clear()

# graph8()

# experiment 5: how much work to utilize a GPU?
# x-axis: MPI ranks
# y-axis: floprate

def graph9():

	# ranks = range(6, 43, 6)
	ranks = range(1, 43)

	small = []
	medium = []
	large = []
	for rank in ranks:
		small.append(float(ut.get_VecDot("../data/vec_ex43/vec_ex43.n1_g6_c" + str(rank) + ".10000.351651")))
		medium.append(float(ut.get_VecDot("../data/vec_ex43/vec_ex43.n1_g6_c" + str(rank) + ".100000.351651")))
		large.append(float(ut.get_VecDot("../data/vec_ex43/vec_ex43.n1_g6_c" + str(rank) + ".10000000.351651")))

	# plot
	num = 2
	cm = plt.get_cmap('Greys')
	fig = plt.figure()
	ax = fig.add_subplot(111)
	ax.set_color_cycle([cm((1.*i+1)/num) for i in range(num)])

	ax.plot(ranks, large, linestyle="none", marker="o", markersize="6", markeredgewidth=2, label="Vec size $10^7$")
	ax.plot(ranks, medium, linestyle="none", marker="o", markersize="6", markeredgewidth=2, label="Vec size $10^5$")
	ax.plot(ranks, small, linestyle="none", marker="o", markersize="6", markeredgewidth=2, label="Vec size $10^4$")

	plt.title("Effect of vector size on performance", fontsize=16) # Vec size $10^6$
	plt.xlabel("Number of MPI ranks", fontsize=16)
	plt.ylabel("VecDot MFlops/second", fontsize=16)
	plt.legend(loc="center right", fontsize=16)
	plt.xlim([0, 43])
	plt.tight_layout()

	plt.show()
	# plt.savefig("../plots/vec_ex43_vec_size.png")
	plt.gcf().clear()

# graph9()

# experiment: how does floprate change with vector size?
# x-axis: vector size
# y-axis: VecDot flop rate
# this is from my program where the first VecDot is timed separately 
# from the loop, we plot them both

def graph10():

	size1 = [1000, 10000, 100000, 1000000, 10000000, 100000000]
	size2 = [200000000, 400000000, 800000000, 1000000000]

	cpu_only = []
	first_vecdot = []
	vecdot_stage = []

	for size in size1:
		cpu_only.append(float(ut.get_VecDot("../data/vecDot_stage/vec_ex43.n6_g0_c2." + str(size) + ".397731", 5)))
		first_vecdot.append(float(ut.get_VecDot("../data/vecDot_stage/vec_ex43.n6_g1_c2." + str(size) + ".387402", 3)))
		vecdot_stage.append(float(ut.get_VecDot("../data/vecDot_stage/vec_ex43.n6_g1_c2." + str(size) + ".387402", 5)))

	for size in size2:
		cpu_only.append(float(ut.get_VecDot("../data/vecDot_stage/vec_ex43.n6_g0_c2." + str(size) + ".397736", 5)))
		first_vecdot.append(float(ut.get_VecDot("../data/vecDot_stage/vec_ex43.n6_g1_c2." + str(size) + ".397734", 3)))
		vecdot_stage.append(float(ut.get_VecDot("../data/vecDot_stage/vec_ex43.n6_g1_c2." + str(size) + ".397734", 5)))

	# plot
	plt.plot(size1 + size2, first_vecdot, linestyle="none", color="firebrick", marker="o", markersize="6", markeredgewidth=1.5, markerfacecolor='none', label="First operation")
	plt.plot(size1 + size2, vecdot_stage, linestyle="none", color="firebrick", marker="x", markersize="6", markeredgewidth=1.5, label="Loop with GPUs")
	plt.plot(size1 + size2, cpu_only, linestyle="none", color="steelblue", marker="x", markersize="6", markeredgewidth=1.5, label="CPU only loop")

	plt.title("Performance with 12 CPUs", fontsize=16)
	plt.xlabel("Vector size", fontsize=16)
	plt.ylabel("VecDot MFlops/second", fontsize=16)
	plt.legend(loc="upper left", fontsize=16)
	plt.tight_layout()
	plt.xscale('log')

	# plt.show()
	plt.savefig("../plots/vecDot_vec_size.png")
	plt.gcf().clear()

graph10()







